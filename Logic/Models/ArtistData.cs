﻿namespace Logic
{
    public class ArtistData
    {
        public int TotalSongsChecked { get; set; }
        public int AverageLyrics { get; set; }
        public int MaxLyrics { get; set; }
        public int MinLyrics { get; set; }
    }
}
