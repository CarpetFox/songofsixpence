﻿using System.Collections.Generic;

namespace Logic
{
    public class ArtistResult
    {
        public List<Artist> artists { get; set; }
    }
}
