﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic
{
    public class ReleaseResult
    {
        public List<Release> releases { get; set; }
    }
}
