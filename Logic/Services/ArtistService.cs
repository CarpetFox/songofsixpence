﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Logic
{
    public class ArtistService
    {
        //these would ideally be configured in an app config file or in the db
        private const string MusicBrainzBaseApi = "https://musicbrainz.org/ws/2/";
        private const string LyricsOvhBaseApi = "https://private-anon-35474b111c-lyricsovh.apiary-proxy.com/v1/";
        private const int NumberOfSongsToAnalyse = 100; //this can be between 1 and 100
        private const int MaxNumberOfArtistsToSearch = 100; //this can be between 1 and 100

        public List<Artist> GetArtists(string searchName)
        {
            if (!string.IsNullOrEmpty(searchName))
            {
                HttpClient client = new HttpClient();
                client.DefaultRequestHeaders.Add("User-Agent", "PostmanRuntime/7.26.5");
                var response = client.GetAsync($"{MusicBrainzBaseApi}artist?query=artist:{searchName}&fmt=json&limit={MaxNumberOfArtistsToSearch}").Result;
                if (response.IsSuccessStatusCode)
                {
                    var result = response.Content.ReadAsStringAsync().Result;
                    var data = JsonSerializer.Deserialize<ArtistResult>(result);
                    return data.artists;
                }
            }

            return new List<Artist>();
        }
        
        public ArtistData GetArtistData(Artist artist)
        {
            //first get releases by that artist
            HttpClient client = new HttpClient();
            //user-agent is required or will get forbidden response
            client.DefaultRequestHeaders.Add("User-Agent", "SongOfSixpence/1.0");
            var response = client.GetAsync($"{MusicBrainzBaseApi}release?query=arid:{artist.id}&fmt=json&limit={NumberOfSongsToAnalyse}&status:Official&offset:0").Result;
            
            if (response.IsSuccessStatusCode)
            {
                //next can parse song/track titles
                var result = response.Content.ReadAsStringAsync().Result;
                var data = JsonSerializer.Deserialize<ReleaseResult>(result);
                var uniqueSongs = data.releases.Select(d => d.title).Distinct().ToList();

                //set up for storing data
                var totalLyrics = 0;
                var maxLyrics = 0;
                var minLyrics = 0;
                var songsWhichReturnedLyrics = 0;

                //begin processing each song
                client = new HttpClient();
                Parallel.ForEach(uniqueSongs, s =>
                {
                    ProcessSong(artist, s, client, ref totalLyrics, ref maxLyrics, ref minLyrics, ref songsWhichReturnedLyrics);
                });

                //map data to be returned
                var artistData = new ArtistData();
                artistData.TotalSongsChecked = songsWhichReturnedLyrics;
                artistData.AverageLyrics = songsWhichReturnedLyrics > 0 ? totalLyrics / songsWhichReturnedLyrics: 0;
                artistData.MaxLyrics = maxLyrics;
                artistData.MinLyrics = minLyrics;

                return artistData;
            }

            return null;
        }

        private void ProcessSong(Artist artist, string s, HttpClient client, ref int totalLyrics, ref int maxLyrics, ref int minLyrics, ref int songsWhichReturnedLyrics)
        {
            var lyricResponse = client.GetAsync($"{LyricsOvhBaseApi}{artist.name}/{s}").Result;
            if (lyricResponse.IsSuccessStatusCode)
            {
                songsWhichReturnedLyrics++;
                var lyricData = lyricResponse.Content.ReadAsStringAsync().Result;

                var lyricNumber = lyricData.Replace("\n", " ").Split(" ").Length;
                totalLyrics += lyricNumber;

                if (lyricNumber > maxLyrics)
                {
                    maxLyrics = lyricNumber;
                }
                if (minLyrics == 0 || lyricNumber < minLyrics)
                {
                    minLyrics = lyricNumber;
                }
            }
        }
    }
}
