# README #

### Initial setup ###

The application is made in Server Side Blazor and targets .Net Core 3.1. It is best run on Visual Studio 2019. There shouldn't be any special steps required provided your version of VS is up to date.

### A note on the architecture ###

I decided to create the UI using Blazor, which is the first time I've actually used it. I was originally going to use Angular, but thought it would be simpler for someone else to run the solution and I also wanted to push myself to try something new.

The business logic classes are in their own Logic project (needs a better name) and are dependency injected into the front end layer, meaning it is easy to switch out the logic or the UI for alternatives.

### How does the application work? ###

1. Run the application from Visual Studio.
2. Type an artists name in the search input. There is a 0.5 second debounce on the input to prevent making too many API calls, but otherwise the search will be triggered automatically.
3. A table of artists matching the name will be returned. This will only include up to 100 artists at present.
4. Select "Analyze Lyrics" of any individual artist, and a chart below will be loaded presenting data on their lyrics.
5. The application currently retrieves up to 100 releases for an artist. Some of these will be duplicates, but typically over 90 are unique. It will then try and find the lyrics for these to aggregate the data.
6. The bar chart shows the number of songs from the initial 100 which returned lyrics successfully, the mean average number of lyrics (averaging based on the number with lyrics, not the initial 100), the maximum number of lyrics found out of those, and the minimum.
7. The bar chart will reload if another artist is chosen, and will clear currently if a new search is performed.

### Considerations for future development / scalability ###

1. The main bottleneck in the process is the lyrics API. It would be worth investigating a different API. One which allows sets of lyric data to be pulled back in a single request would improve efficiency.
2. Currently the default JSON serializer is used. There are probably faster alternatives out there, and writing a custom one too may speed up the process.
3. It would be good to cache the results to speed up similar requests. Adding its own API layer to be called by the UI, which will then in turn call the business logic may be a better architecture because controllers would give better control of caching behaviour.
4. For greater scalability it may be worth saving the calculated data in a database. Would only need to check if there have been any recent releases and recalculate data if there have been in that case, vastly speeding up the process for common searches.
5. The UI is currently optimised for a 1080p screen specifically. It does rezise okay to some extent, but to be viewed on mobile properly the min-width css should be replaced on components of the MatTable. The inline css also needs extracting out as well as other code quality imrpovements.
6. Server Side Blazor may not be the best approach if this was used on a larger scale, so should consider swapping it out for a different option.
7. In its current format, the charts are loaded in a Blazor component. This means that it is fairly simple to extend at this point to show multiple charts at the same time. A spider diagram would be a good candidate for showing multiple artists' lyric data at the same time.
8. Currently a large percentage of the 100 initially checked songs do not return lyrics (40-90%). More investigation needs doing into the APIs to see if there are ways to either exclude unusual releases, or a way to find lyrics for those.
9. To return more than 100 releases from a single artists would currently involve multiple API calls to Musicbrainz, incrementing the offset. Musicbrainz seem quite conscious of applications making too many calls to their API, and warn against blacklisting, which needs careful consideration for any future scalability.
