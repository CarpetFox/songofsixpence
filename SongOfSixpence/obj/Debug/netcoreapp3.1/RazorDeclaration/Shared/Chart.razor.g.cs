#pragma checksum "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\Shared\Chart.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "27b158696a5b9dbac6411c5f81e726d99202de95"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace SongOfSixpence.Shared
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using SongOfSixpence;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using SongOfSixpence.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\_Imports.razor"
using MatBlazor;

#line default
#line hidden
#nullable disable
    public partial class Chart : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 5 "C:\Users\Joshu\source\repos\Blazor\SongOfSixpence\SongOfSixpence\Shared\Chart.razor"
       
    public enum ChartType
    {
        Pie,
        Bar
    }

    [Parameter]
    public string Id { get; set; }

    [Parameter]
    public ChartType Type { get; set; }

    [Parameter]
    public string[] Data { get; set; }

    [Parameter]
    public string[] BackgroundColor { get; set; }

    [Parameter]
    public string[] Labels { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        //Here we create an anonymous type with all the options that need to be sent to Chart.js
        var config = new
        {
            Type = Type.ToString().ToLower(),
            Options = new
            {
                Responsive = true,
                Scales = new
                {
                    YAxes = new[]
                    {
                        new { Ticks = new {
                            BeginAtZero=true
                        } }
                    }
                },
                Legend = new
                {
                    Display = false
                }
            },
            Data = new
            {
                Datasets = new[]
                {
                    new { Data = Data, BackgroundColor = BackgroundColor}
                },
                Labels = Labels
            }
        };

        await JSRuntime.InvokeVoidAsync("setup", Id, config);
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
